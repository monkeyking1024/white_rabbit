<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
        $fileContents=file_get_contents($filePath) or die("Unable to read file ($filePath)!");
        $counter=array();
        foreach (range("a","z") as $ltr) {
            $counter[$ltr]=substr_count(strtolower($fileContents), $ltr);
        }
        return $counter;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
        asort($parsedFile, SORT_NUMERIC);
        $letters=array_keys($parsedFile);
        $medianLetter=$letters[count($letters)/2];
        $occurrences=$parsedFile[$medianLetter];
        return $medianLetter;
    }
}