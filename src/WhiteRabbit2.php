<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        is_int($amount) && $amount>=0 or die("The amount should be an nonnegative integer!");
        $coins=array('100' => 0,'50' => 0,'20' => 0,'10' => 0,'5' => 0,'2' => 0,'1' => 0);
        foreach ($coins as $coin => $number) {
            $coinValue=intval($coin);
            $coins[$coin]=intval(floor($amount/$coinValue));
            $amount-=$coins[$coin]*$coinValue;
            if ($amount<=0) break;
        }
        return $coins;
    }
}